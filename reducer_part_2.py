#!/usr/bin/python

import sys

salesTotal = 0
oldKey = None
NumberOfOcuurence = 0
MostNumberOfOcuurence = 0
MostPopularFilePath = ''

# Loop around the data
# It will be in the format key\tval
# Where key is the store name, val is the sale amount
#
# All the sales for a particular store will be presented,
# then the key will change and we'll be dealing with the next store

for line in sys.stdin:
    data_mapped = line
    if len(data_mapped) <=1:
        # Something has gone wrong. Skip this line.
        continue

    thisKey = data_mapped

    if oldKey and oldKey != thisKey:
	#print oldKey, "\t",'ans=' ,NumberOfOcuurence
        
        if NumberOfOcuurence > MostNumberOfOcuurence:
            MostNumberOfOcuurence = NumberOfOcuurence
            MostPopularFilePath = oldKey
	oldKey = thisKey
        NumberOfOcuurence = 0




    oldKey = thisKey
    NumberOfOcuurence += 1


if oldKey != None:
    if NumberOfOcuurence > MostNumberOfOcuurence:
	MostNumberOfOcuurence = NumberOfOcuurence
	MostPopularFilePath = oldKey
    print MostPopularFilePath, "\t",'ans=' ,MostNumberOfOcuurence


