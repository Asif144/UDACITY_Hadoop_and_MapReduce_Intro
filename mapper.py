#!/usr/bin/python

# Format of each line is:
# date\ttime\tstore name\titem description\tcost\tmethod of payment
#
# We want elements 2 (store name) and 4 (cost)
# We need to write them out to standard output, separated by a tab

import sys

path_strip = "http://www.the-associates.co.uk"

for line in sys.stdin:
	data = line.strip().split(" ")
	if (len(data) == 10):
		ip, ident, username, date, tz, method, path, query, status, size = data
		if (path_strip in path):
			path = path.replace(path_strip, '')
		print path