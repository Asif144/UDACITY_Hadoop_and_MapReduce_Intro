import re
s="10.223.157.186 - - [15/Jul/2009:15:50:35 -0700] \"GET /assets/js/lowpro.js HTTP/1.1\" 200 10469"
s= (s.split("\""))
if len(s)==3:
    Req = s[1]
    Req = Req.replace("GET",'')
    Req = Req.strip()
    Req = Req.split("HTTP")[0]
    Req = Req.strip()
    print(Req)


st = "Hey!\nTwo sentences!"
st = st.replace("\n",'')
idx = st.index('o')
print(idx)

st = st.lower()
print(st)


'''
preprocessing for L_7_Q_7
'''
test_str = "<p>what is the difference between [S] and [s+s) ? are they     the same </p>"
test_str = re.sub('<.*?>','',test_str)
test_str = re.sub('[^A-Za-z ]+', '', test_str)
test_str = test_str.strip()
test_str = test_str.lower()
print(test_str)


'''
L_7_Q_8
'''

from datetime import datetime
date = "2017-10-22"
weekday = datetime.strptime(date, "%Y-%m-%d").weekday()
print('day=',weekday,"\t",'hey')
print('{0}\t{1}'.format(weekday,'name'))



'''
final project
'''
dt = "2012-02-25 08:09:06.787181+00"
dt = dt[0:len(dt)-3]
print(dt)
dt = datetime.strptime(dt, "%Y-%m-%d %H:%M:%S.%f")
hour = dt.hour
print(hour)
