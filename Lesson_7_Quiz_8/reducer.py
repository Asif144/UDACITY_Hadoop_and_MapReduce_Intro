#!/usr/bin/python

# Format of each line is:
# date\ttime\tstore name\titem description\tcost\tmethod of payment
#
# We want elements 2 (store name) and 4 (cost)
# We need to write them out to standard output, separated by a tab

import sys
oldKey = None
totalCost = 0.0
totalOccurance = 0
meanForEach = 0.0

for line in sys.stdin:
    data = line.strip().split("\t")
    if len(data) != 2:
        continue
    thisKey, thisCost = data
    if oldKey and oldKey!=thisKey:
        try:
            meanForEach = totalCost/totalOccurance
        except:
            pass
        print('weekday=', oldKey,'mean=', meanForEach)
        meanForEach = 0.0
        totalOccurance = 0
        totalCost = 0.0
    oldKey = thisKey
    totalCost = totalCost + float(thisCost)
    totalOccurance += 1

if oldKey!=None:
    try:
        meanForEach = totalCost / totalOccurance
    except:
        pass
    print('weekday=', oldKey, 'mean=', meanForEach)
